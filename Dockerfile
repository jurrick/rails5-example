# FROM ubuntu:16.04
FROM ruby:2.3.3

RUN apt update -qq && apt install -y nodejs npm

RUN mkdir /rails5
WORKDIR /rails5
ADD Gemfile /rails5/Gemfile
ADD Gemfile.lock /rails5/Gemfile.lock
RUN gem install bundler
RUN gem install rb-readline
RUN bundle install --jobs $(nproc)
ADD . /rails5

CMD ["bundle exec rails s"]
